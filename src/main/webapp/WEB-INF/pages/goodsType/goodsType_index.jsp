<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>物品类型列表</title>
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
</head>

<h1>物品类型列表</h1>

<select id="goodsType">
    <option value="-1" selected>全部</option>
    <c:forEach items="${data}" var="goodsType">
        <option value="${goodsType.id}">${goodsType.typeName}</option>
    </c:forEach>
</select>

<button id="searchBtn">查询</button>

<div id="content"></div>


<script type="text/javascript">

    $(document).ready(function () {

        // 获得项目根路径
        var base_url = <c:url value="/"></c:url>

        $("#searchBtn").click(function () {

            // 发送ajax，获得返回数据
            $.ajax({
                url: base_url + "goodsType/getGoodsTypeList",
                data: {
                    id: $("#goodsType").val()
                },
                method: "post",
                success: function (e) {
                    var html = "";
                    for(var i = 0; i < e.length; i++) {
                        html += e[i].id + " ";
                        html += e[i].typeName + " ";
                        html += e[i].isDel + "<br>";
                        $("#content").html(html);
                    }
                },
                error: function () {
                    alert("error");
                }
            });
        });
    });

</script>
</body>
</html>
