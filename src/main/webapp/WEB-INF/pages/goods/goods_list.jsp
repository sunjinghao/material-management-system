<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>物资管理系统 - 物品列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <%@include file="../common.jsp"%>

</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">物品名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="goodsName" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label">物品类型</label>
                            <div class="layui-input-inline">
                                <select name="typeId">
                                    <option value="-1" selected>全部</option>
                                    <c:forEach items="${data}" var="goodsType">
                                        <option value="${goodsType.id}">${goodsType.typeName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加 </button>
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" lay-event="delete"> 删除 </button>
                <a href="<c:url value="/template/goods_template.xls" />" class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" > 导入模版 </a>
                <a href="<c:url value="/goods/excelExport" />" class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" > 物品导出 </a>
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" id="test1">物品导入</button>
            </div>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>

<script>
    layui.use(['upload', 'form', 'table'], function () {

        var $ = layui.jquery,
            form = layui.form,
            table = layui.table;

        table.render({
            elem: '#currentTableId',
            url: base_url + 'goods/list',
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {title: '序号', type: 'numbers'},
                {field: 'goodsName', title: '物品名称'},
                {field: 'count', title: '库存', sort: true},
                {field: 'typeName', title: '物品类型'},
                {title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 10,
            page: true,
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {

            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: data.field
            }, 'data');

            return false;
        });

        /**
         * toolbar监听事件
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {
                // 监听添加操作
                var index = layer.open({
                    title: '添加物品',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['100%', '100%'],
                    content: '<c:url value="/goods/toAdd.do" />',
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            } else if (obj.event === 'delete') {
                // 批量删除操作
                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;

                $.ajax({
                    "url": base_url + "goods/deleteGoodsList.do",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    "data": JSON.stringify(data),
                    "method": "post",
                    success: function () {
                        layer.alert("批量删除成功");
                    },
                    error: function () {
                        layer.alert("批量删除失败");
                    }
                });
            }
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            console.log(data);
            if (obj.event === 'edit') {

                var index = layer.open({
                    title: '编辑物品',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['100%', '100%'],
                    content: '<c:url value="/goods/toUpdate.do" />' + "?id=" + data.id,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('真的删除行么', function (index) {
                    // 删除
                    $.ajax({
                        "url": base_url + "goods/deleteGoods.do",
                        "data": {
                            "id": data.id
                        },
                        "method": "post",
                        success: function() {
                            obj.del();
                            layer.close(index);
                        },
                        error: function () {
                            layer.close(index);
                        }
                    });
                });
            }
        });

    });
</script>

<script type="text/javascript">
    layui.use(['upload', 'table'], function () {
        var upload = layui.upload;
        var mask;
        //执行实例
        upload.render({
            elem: "#test1",
            url: base_url + 'goods/excelImport',
            accept: 'file',
            exts: 'xls|xlsx',
            before: function (res) {
                mask = layer.load(0, {
                    shade: [1, '#fff'] //不透明的白色背景
                });
            },
            done: function (res) {
                if (document.readyState != 'loading') {    //循环判断document状态，如果加载完成则表示开始了下载，此时关闭提示窗口
                    layer.close(mask);
                }
            }
        })
    });

</script>

</body>
</html>