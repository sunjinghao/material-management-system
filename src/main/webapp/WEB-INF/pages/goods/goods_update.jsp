<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <%@include file="../common.jsp"%>
    
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form layuimini-form">
    <div class="layui-form-item">
        <label class="layui-form-label required">物品名称</label>
        <div class="layui-input-block">
            <input type="text" name="goodsName" lay-verify="required" lay-reqtext="物品名称不能为空" placeholder="请输入物品名称" value="${data.goods.goodsName}" class="layui-input">
            <%--            <tip>填写添加物品的名称。</tip>--%>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">数量</label>
        <div class="layui-input-block">
            <input type="number" name="count" lay-verify="required" lay-reqtext="数量不能为空" placeholder="请输入数量" value="${data.goods.count}" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">物品类型</label>
        <div class="layui-input-block">
            <select name="typeId">
                <c:forEach items="${data.goodsTypeList}" var="goodsType">
                    <c:choose>
                        <c:when test="${goodsType.id == data.goods.typeId}">
                            <option value="${goodsType.id}" selected>${goodsType.typeName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${goodsType.id}">${goodsType.typeName}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">备注信息</label>
        <div class="layui-input-block">
            <textarea name="remark" class="layui-textarea" placeholder="请输入备注信息" value="${data.goods.remark}"></textarea>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
        </div>
    </div>
</div>
</div>

<script>
    layui.use(['form'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.$;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            var index = layer.alert(JSON.stringify(data.field), {
                title: '最终的提交信息'
            }, function () {

                // 关闭弹出层
                layer.close(index);

                var iframeIndex = parent.layer.getFrameIndex(window.name);
                parent.layer.close(iframeIndex);

            });

            return false;
        });

    });
</script>
</body>
</html>