<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link rel="stylesheet" href="<c:url value="/lib/layui-v2.6.3/css/layui.css"></c:url>" media="all">
<link rel="stylesheet" href="<c:url value="/css/public.css"></c:url>" media="all">
<script src="<c:url value="/lib/layui-v2.6.3/layui.js"></c:url>" charset="utf-8"></script>

<script type="text/javascript">
    var base_url = <c:url value="/"></c:url>
</script>