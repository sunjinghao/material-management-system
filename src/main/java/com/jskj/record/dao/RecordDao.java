package com.jskj.record.dao;

import com.jskj.record.entity.Record;

public interface RecordDao {

    /**
     * 添加出库记录
     * @param record
     * @return
     */
    Integer addRecord(Record record);
}
