package com.jskj.record.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Record {
    private Integer id;
    private Integer userId;
    private Integer goodsId;
    private Integer count;

    private Date createDate;
    private Date createTime;
}
