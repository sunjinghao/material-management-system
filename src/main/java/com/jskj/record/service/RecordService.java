package com.jskj.record.service;

import com.jskj.goods.entity.Goods;
import com.jskj.record.entity.Record;

public interface RecordService {

    /**
     * 物品出库
     * @param record
     * @param goods
     */
    void checkOut(Record record, Goods goods);
}
