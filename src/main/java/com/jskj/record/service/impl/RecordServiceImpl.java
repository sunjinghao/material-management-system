package com.jskj.record.service.impl;

import com.jskj.goods.dao.GoodsDao;
import com.jskj.goods.entity.Goods;
import com.jskj.record.entity.Record;
import com.jskj.record.dao.RecordDao;
import com.jskj.record.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RecordServiceImpl implements RecordService {

    @Autowired
    private RecordDao recordDao;

    @Autowired
    private GoodsDao goodsDao;

    @Transactional
    @Override
    public void checkOut(Record record, Goods goods) {
        // 添加出库记录
        recordDao.addRecord(record);
        int a = 1 / 0;
        // 扣减库存
        goodsDao.updateGoodsCountById(goods);
    }
}
