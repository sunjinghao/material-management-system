package com.jskj.record.controller;

import com.jskj.goods.entity.Goods;
import com.jskj.goods.service.GoodsService;
import com.jskj.record.entity.Record;
import com.jskj.record.service.RecordService;
import com.jskj.system.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("record")
public class RecordController {

    @Autowired
    private RecordService recordService;

    @Autowired
    private GoodsService goodsService;

    @RequestMapping("checkOut")
    @ResponseBody
    public Map<String, Object> checkOut(@RequestBody Record record) {

        Map<String, Object> result = new HashMap<>();

        try {
            // 计算出库后的数量
            Goods goods = new Goods();
            goods.setId(record.getGoodsId());
            Goods goodsInDb = goodsService.getGoodsInfoById(goods);

            // 判断库存是否足够
            if(goodsInDb != null && goodsInDb.getCount() >= record.getCount()) {

                goods.setCount(goodsInDb.getCount() - record.getCount());
                // 出库
                recordService.checkOut(record, goods);

                result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
                result.put(Constants.RETURN_MSG, Constants.MSG_SUCCESS);
            }else {
                result.put(Constants.RETURN_CODE, Constants.CODE_FAIL);
                result.put(Constants.RETURN_MSG, Constants.PARAM_ERROR);
            }

        }catch (Exception e) {
            e.printStackTrace();
            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, Constants.MSG_ERROR);
        }

        return result;
    }


}
