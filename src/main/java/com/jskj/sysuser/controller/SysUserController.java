package com.jskj.sysuser.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.github.pagehelper.util.StringUtil;
import com.jskj.system.Constants;
import com.jskj.sysuser.entity.SysUser;
import com.jskj.sysuser.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("sysuser")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("login.do")
    @ResponseBody
    public Map login(SysUser sysUser) {

        Map<String, Object> result = new HashMap<>();

        // 用户名密码不能为空
        if(StrUtil.isEmpty(sysUser.getUsername()) || StringUtil.isEmpty(sysUser.getPassword())) {
            result.put(Constants.RETURN_CODE, Constants.CODE_FAIL);
            result.put(Constants.RETURN_MSG, Constants.MSG_ERROR);
            return result;
        }

        // 调用service，去查找用户是否存在
        SysUser sysUserInDb = sysUserService.check(sysUser);
        // 比较加密后的密码是否相等
        String encrypPassword = SecureUtil.md5(sysUser.getPassword());

        if(sysUserInDb != null && sysUserInDb.getPassword().equals(encrypPassword)) {
            result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
            result.put(Constants.RETURN_MSG, Constants.MSG_SUCCESS);
        }else {
            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, Constants.MSG_ERROR);
        }

        return result;
    }

}
