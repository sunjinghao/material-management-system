package com.jskj.sysuser.service;

import com.jskj.sysuser.entity.SysUser;

public interface SysUserService {

    /**
     * 校验用户是否存在
     * @param sysUser
     * @return
     */
    SysUser check(SysUser sysUser);
}
