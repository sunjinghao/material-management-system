package com.jskj.sysuser.service.impl;

import com.jskj.sysuser.dao.SysUserDao;
import com.jskj.sysuser.entity.SysUser;
import com.jskj.sysuser.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public SysUser check(SysUser sysUser) {
        return sysUserDao.getSysUserByUserName(sysUser);
    }
}
