package com.jskj.sysuser.dao;

import com.jskj.sysuser.entity.SysUser;

public interface SysUserDao {

    /**
     * 根据用户名查找用户
     * @param sysUser
     * @return
     */
    SysUser getSysUserByUserName(SysUser sysUser);
}
