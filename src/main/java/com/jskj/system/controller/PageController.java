package com.jskj.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 通用页面跳转
 */
@Controller
@RequestMapping("pages")
public class PageController {

    @RequestMapping("{packageName}/{pageName}")
    @ResponseBody
    public ModelAndView getBasicPages(@PathVariable("packageName") String packageName,
                                      @PathVariable("pageName") String pageName) {

        System.out.println(packageName);
        System.out.println(pageName);

        return new ModelAndView(packageName + "/" + pageName);
    }

    @RequestMapping("{pageName}")
    @ResponseBody
    public ModelAndView getBasicPages(@PathVariable("pageName") String pageName) {
        System.out.println(pageName);
        return new ModelAndView(pageName);
    }
}
