package com.jskj.system;

public class Constants {

    public static final String RETURN_CODE = "code";
    public static final String RETURN_MSG = "msg";
    public static final String RETURN_DATA = "data";
    public static final String RETURN_COUNT = "count";

    public static final String CODE_SUCCESS = "0";
    public static final String CODE_ERROR = "1";
    public static final String CODE_FAIL = "2";

    public static final String MSG_SUCCESS = "请求成功";
    public static final String MSG_ERROR = "请求失败";
    public static final String PARAM_ERROR = "参数异常";

}
