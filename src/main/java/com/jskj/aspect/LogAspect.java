package com.jskj.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @Component交给spring管理
 * 开启切面
 */
//@Component
//@Aspect
public class LogAspect {

    @Pointcut("execution(* com.jskj.service.impl.CountServiceImpl.*(..))")
    public void pointCut() {

    }

//    /**
//     * 前置通知
//     * execution 切点
//     * joinPoint 连接点
//     */
//    @Before("pointCut()")
//    public void beforeAdvice(JoinPoint jp) {
//        String methodName = jp.getSignature().getName();
//        List<Object> args = Arrays.asList(jp.getArgs());
//        System.out.println("enter method [" + methodName + "], args is " + args);
//    }
//
//    /**
//     * 返回通知，可以获得执行结果
//     * @param jp
//     * @param result
//     */
//    @AfterReturning(value = "pointCut()", returning = "result")
//    public void afterReturningAdvice(JoinPoint jp, Object result) {
//        String methodName = jp.getSignature().getName();
//        System.out.println("[" + methodName + "] 返回结果为:" + result);
//    }
//
//    /**
//     * 异常通知
//     * @param jp
//     * @param ex
//     */
//    @AfterThrowing(value = "pointCut()", throwing = "ex")
//    public void afterThrowing(JoinPoint jp, Exception ex) {
//        String methodName = jp.getSignature().getName();
//        System.out.println("[" + methodName + "] 方法异常:" + ex);
//    }
//
//    /**
//     * 后置通知
//     * @param jp
//     */
//    @After("pointCut()")
//    public void afterAdvice(JoinPoint jp) {
//        String methodName = jp.getSignature().getName();
//        System.out.println("after method [" + methodName +"]");
//    }

    /**
     * 环绕通知
     */
    @Around("pointCut()")
    public Object around(ProceedingJoinPoint pjp) {

        String methodName = pjp.getSignature().getName();
        List<Object> args = Arrays.asList(pjp.getArgs());
        Object result = null;

        try {
            // 前置通知
            System.out.println("enter method [" + methodName + "], args is " + args);

            // 执行方法
            result = pjp.proceed();

            // 返回通知
            System.out.println("[" + methodName + "] 返回结果为:" + result);
        }catch (Throwable e) {
            System.out.println("[" + methodName + "] 方法异常:" + e);
        }

        System.out.println("after method [" + methodName +"]");
        return result;
    }
}
