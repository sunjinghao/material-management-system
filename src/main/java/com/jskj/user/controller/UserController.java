package com.jskj.user.controller;

import com.jskj.user.entity.SysUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("user")
public class UserController {

    @RequestMapping("login")
    public String login(SysUser sysUser) {
        System.out.println("login");
        return "login";
    }

    @RequestMapping("register")
    public String register() {
        return "";
    }
}
