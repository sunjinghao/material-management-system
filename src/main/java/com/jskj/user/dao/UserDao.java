package com.jskj.user.dao;

import com.jskj.user.entity.SysUser;

public interface UserDao {

    /**
     * 通过用户名和密码查询用户
     * @return
     */
    SysUser getSysUserFromDB(SysUser sysUser);

    /**
     * 添加用户
     * @param sysUser
     * @return
     */
    int addSysUser(SysUser sysUser);

}
