package com.jskj.goods.dao;

import com.jskj.goods.entity.GoodsType;

import java.util.List;

public interface GoodsTypeDao {

    /**
     * 获得物品类型列表
     * @return
     */
    List<GoodsType> getGoodsTypeList(GoodsType goodsType);
}
