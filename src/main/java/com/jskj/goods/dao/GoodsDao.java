package com.jskj.goods.dao;

import com.jskj.goods.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodsDao {

    /**
     * 获得物品列表
     * @return
     */
    List<Goods> getGoodsListByPage(Goods goods);

    /**
     * 通过id获得物品信息
     * @param goods
     * @return
     */
    Goods getGoodsInfoById(Goods goods);

    /**
     * 添加物品
     * @param goods
     * @return
     */
    int addGoods(Goods goods);

    /**
     * 删除物品
     * @param goods
     * @return
     */
    int deleteGoods(Goods goods);

    /**
     * 批量删除
     * @param goodsList
     * @return
     */
    int deleteGoodsList(@Param("goodsList") List<Goods> goodsList);

    /**
     * 通过修改物品
     * @param goods
     * @return
     */
    int updateGoodsById(Goods goods);
}
