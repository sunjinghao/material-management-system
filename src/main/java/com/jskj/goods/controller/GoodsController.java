package com.jskj.goods.controller;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.jskj.goods.entity.Goods;
import com.jskj.goods.entity.GoodsType;
import com.jskj.goods.service.GoodsService;
import com.jskj.goods.service.GoodsTypeService;
import com.jskj.system.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("goods")
public class GoodsController {

    private static Logger logger = LogManager.getLogger(GoodsController.class);

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private GoodsTypeService goodsTypeService;

    @RequestMapping("index.do")
    public ModelAndView index(Goods goods) {

        Map<String, Object> result = new HashMap<>();

        // 调用service层
        List<GoodsType> goodsTypeList = goodsTypeService.getGoodsTypeList(new GoodsType());

        result.put(Constants.RETURN_DATA, goodsTypeList);
        result.put(Constants.RETURN_CODE, "1");
        result.put(Constants.RETURN_MSG, "请求成功");

        return new ModelAndView("/goods/goods_list", result);
    }

    /**
     * 获得物品列表
     * @param goods
     * @return
     */
    @RequestMapping("list")
    @ResponseBody
    public Map list(Goods goods) {

        logger.info(goods);

        Map<String, Object> result = new HashMap<>();

        // 分页对象
        PageInfo<Goods> pageInfo = new PageInfo<>(goodsService.getGoodsListByPage(goods));

        result.put(Constants.RETURN_DATA, pageInfo.getList());
        result.put(Constants.RETURN_COUNT, pageInfo.getTotal());

        result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
        result.put(Constants.RETURN_MSG, "请求成功");

        return result;
    }


    /**
     * 删除物品
     * @return
     */
    @RequestMapping("deleteGoods.do")
    @ResponseBody
    public Map<String, Object> deleteGoods(Goods goods) {
        Map<String, Object> result = new HashMap<>();

        if(goodsService.deleteGoods(goods) > 0) {
            result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
            result.put(Constants.RETURN_MSG, "删除成功");
        }else {
            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, "删除成功");
        }
        return result;
    }

    /**
     * 批量删除物品
     * @return
     */
    @RequestMapping("deleteGoodsList.do")
    @ResponseBody
    public Map<String, Object> deleteGoodsList(@RequestBody List<Goods> goodsList) {
        Map<String, Object> result = new HashMap<>();

        if(goodsService.deleteGoodsList(goodsList) > 0) {
            result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
            result.put(Constants.RETURN_MSG, "删除成功");
        }else {
            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, "删除成功");
        }
        return result;
    }

    @RequestMapping("toAdd.do")
    public ModelAndView toAdd(Goods goods) {

        Map<String, Object> result = new HashMap<>();

        // 调用service层
        List<GoodsType> goodsTypeList = goodsTypeService.getGoodsTypeList(new GoodsType());

        result.put(Constants.RETURN_DATA, goodsTypeList);
        result.put(Constants.RETURN_CODE, "1");
        result.put(Constants.RETURN_MSG, "请求成功");

        return new ModelAndView("/goods/goods_add", result);
    }

    /**
     * 添加物品
     * @param goods
     * @return
     */
    @RequestMapping("addGoods.do")
    @ResponseBody
    public Map<String, Object> addGoods(Goods goods) {

        Map<String, Object> result = new HashMap<>();

        // 校验参数
        if(StrUtil.isEmpty(goods.getGoodsName()) || goods.getCount() == null
                || goods.getTypeId() == null) {

            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, Constants.PARAM_ERROR);

            return result;
        }

        try {
            // 业务层写在try catch块中
            goodsService.addGoods(goods);
            result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
            result.put(Constants.RETURN_MSG, Constants.MSG_SUCCESS);

        }catch (Exception e) {
            e.printStackTrace();
            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, Constants.MSG_ERROR);
        }

        return result;
    }

    @RequestMapping("toUpdate.do")
    public ModelAndView toUpdate(Goods goods) {

        Map<String, Object> result = new HashMap<>();

        // 调用service层
        Map<String, Object> data = new HashMap<>();
        data.put("goodsTypeList", goodsTypeService.getGoodsTypeList(new GoodsType()));
        data.put("goods", goodsService.getGoodsInfoById(goods));

        result.put(Constants.RETURN_DATA, data);
        result.put(Constants.RETURN_CODE, "1");
        result.put(Constants.RETURN_MSG, "请求成功");

        return new ModelAndView("/goods/goods_update", result);
    }

    /**
     * 编辑物品
     * @param goods
     * @return
     */
    @RequestMapping("udpateGoods")
    @ResponseBody
    public Map<String, Object> udpateGoods(Goods goods) {

        Map<String, Object> result = new HashMap<>();

        // 校验参数
        if(StrUtil.isEmpty(goods.getGoodsName()) || goods.getCount() == null
                || goods.getId() == null
                || goods.getTypeId() == null) {

            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, Constants.PARAM_ERROR);

            return result;
        }

        try {
            // 业务层写在try catch块中
            goodsService.updateGoods(goods);
            result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
            result.put(Constants.RETURN_MSG, Constants.MSG_SUCCESS);

        }catch (Exception e) {
            e.printStackTrace();
            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, Constants.MSG_ERROR);
        }

        return result;
    }

    /**
     * excel导入
     * @param file
     * @return
     */
    @RequestMapping("excelImport")
    @ResponseBody
    public Map<String, Object> excelImport(@RequestParam("file") MultipartFile file) {

        Map<String, Object> result = new HashMap<>();

        System.out.println(file.getName());

        try {
            result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
            result.put(Constants.RETURN_MSG, Constants.MSG_SUCCESS);

        }catch (Exception e) {
            e.printStackTrace();
            result.put(Constants.RETURN_CODE, Constants.CODE_ERROR);
            result.put(Constants.RETURN_MSG, Constants.MSG_ERROR);
        }

        return result;
    }

    /**
     * 报表导出
     * @param goods
     * @return
     */
    @RequestMapping("excelExport")
    @ResponseBody
    public void excelExport(Goods goods, HttpServletRequest request, HttpServletResponse response) {

        OutputStream outputStream = null;

        try {

            List<Goods> goodsList = goodsService.getGoodsListByPage(goods);

            String fileName = "物品列表.xls";
            String userAgent = request.getHeader("USER-AGENT").toLowerCase();
            String outputFileName = fileName;
            if (userAgent.toUpperCase().indexOf("MSIE") > 0) {
                outputFileName = URLEncoder.encode(fileName, "UTF-8");
            } else if (userAgent.toUpperCase().indexOf("IPHONE") > 0) {
                outputFileName = new String(fileName.getBytes(), "ISO-8859-1");
            } else {
                outputFileName = new String(fileName.getBytes("utf-8"), "ISO-8859-1");
            }
            String[] titles = {"物品名称", "物品类型", "库存"};

            // 2.导出excel，需要标题栏，需要数据
            HSSFWorkbook wb = goodsService.goodsExport(titles, goodsList);

            response.setContentType("application/octet-stream");
            // 以附件的形式下载
            response.setHeader("Content-disposition", "attachment; filename=\"" + outputFileName + "\"");

            outputStream = response.getOutputStream();
            wb.write(outputStream);

            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
