package com.jskj.goods.controller;

import com.jskj.goods.entity.GoodsType;
import com.jskj.goods.service.GoodsTypeService;
import com.jskj.system.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 跳转至goodsType_index页面
     * @return
     */
    @RequestMapping("index")
    public ModelAndView index() {
        Map<String, Object> result = new HashMap<>();

        List<GoodsType> goodsTypeList = goodsTypeService.getGoodsTypeList(new GoodsType());

        result.put(Constants.RETURN_DATA, goodsTypeList);
        result.put(Constants.RETURN_CODE, Constants.CODE_SUCCESS);
        result.put(Constants.RETURN_MSG, "请求成功");

        return new ModelAndView("goodsType_index", result);
    }

    /**
     * 获得物品类型
     * @param goodsType
     * @return
     */
    @RequestMapping("getGoodsTypeList")
    @ResponseBody
    public List<GoodsType> getGoodsTypeList(GoodsType goodsType) {
        System.out.println(goodsType);
        List<GoodsType> goodsTypeList = goodsTypeService.getGoodsTypeList(goodsType);
        return goodsTypeList;
    }
}
