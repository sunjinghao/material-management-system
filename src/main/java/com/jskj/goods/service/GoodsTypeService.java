package com.jskj.goods.service;

import com.jskj.goods.entity.GoodsType;

import java.util.List;

public interface GoodsTypeService {

    /**
     * 获得物品类型
     * @return
     */
    List<GoodsType> getGoodsTypeList(GoodsType goodsType);
}
