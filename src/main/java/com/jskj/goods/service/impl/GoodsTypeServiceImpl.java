package com.jskj.goods.service.impl;

import com.jskj.goods.dao.GoodsTypeDao;
import com.jskj.goods.entity.GoodsType;
import com.jskj.goods.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public List<GoodsType> getGoodsTypeList(GoodsType goodsType) {
        return goodsTypeDao.getGoodsTypeList(goodsType);
    }
}
