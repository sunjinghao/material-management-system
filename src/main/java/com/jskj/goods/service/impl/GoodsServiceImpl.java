package com.jskj.goods.service.impl;

import com.github.pagehelper.PageHelper;
import com.jskj.goods.dao.GoodsDao;
import com.jskj.goods.entity.Goods;
import com.jskj.goods.service.GoodsService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public List<Goods> getGoodsListByPage(Goods goods) {
        if(goods.getPage() != null && goods.getLimit() != null) {
            PageHelper.startPage(goods.getPage(), goods.getLimit());
        }
        return goodsDao.getGoodsListByPage(goods);
    }

    @Override
    public Goods getGoodsInfoById(Goods goods) {
        return goodsDao.getGoodsInfoById(goods);
    }

    @Override
    public Integer deleteGoods(Goods goods) {
        return goodsDao.deleteGoods(goods);
    }

    @Override
    public Integer deleteGoodsList(List<Goods> goodsList) {
        return goodsDao.deleteGoodsList(goodsList);
    }

    @Override
    public Integer addGoods(Goods goods) {
        return goodsDao.addGoods(goods);
    }

    @Override
    public Integer updateGoods(Goods goods) {
        return goodsDao.updateGoodsById(goods);
    }

    @Override
    public HSSFWorkbook goodsExport(String[] titles, List<Goods> goodsList) {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();

//        CellStyle style = wb.createCellStyle();
////        style.setAlignment(HorizontalAlignment.CENTER); // 创建一个居中格式
//        style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREEN.getIndex());
//        style.setBorderBottom(BorderStyle.THIN);
//        style.setBorderLeft(BorderStyle.THIN);
//        style.setBorderRight(BorderStyle.THIN);
//        style.setBorderTop(BorderStyle.THIN);

        HSSFRow head = sheet.createRow(0);
        for(int i = 0; i < titles.length; i++) {
            HSSFCell cell = head.createCell(i);
            cell.setCellValue(titles[i]);
        }

        for(int i = 0; i < goodsList.size(); i++) {
            HSSFRow row = sheet.createRow(i + 1);
            row.setHeight((short) 350);
            Goods goods = goodsList.get(i);

            HSSFCell cell1 = row.createCell(0);
//            cell1.setCellStyle(style);
            cell1.setCellValue(goods.getGoodsName());

            HSSFCell cell2 = row.createCell(1);
            cell2.setCellValue(goods.getTypeName());

            HSSFCell cell3 = row.createCell(2);
            cell3.setCellValue(goods.getCount());
        }

        return wb;
    }


}
