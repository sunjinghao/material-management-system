package com.jskj.goods.service;

import com.jskj.goods.entity.Goods;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

public interface GoodsService {

    /**
     * 分页获得物品列表
     * @param goods
     * @return
     */
    List<Goods> getGoodsListByPage(Goods goods);

    /**
     * 通过id获得物品信息
     * @param goods
     * @return
     */
    Goods getGoodsInfoById(Goods goods);

    /**
     * 删除物品
     * @param goods
     * @return
     */
    Integer deleteGoods(Goods goods);

    /**
     * 批量删除
     * @param goodsList
     * @return
     */
    Integer deleteGoodsList(List<Goods> goodsList);

    /**
     * 添加物品
     * @param goods
     * @return
     */
    Integer addGoods(Goods goods);

    /**
     * 编辑物品
     * @param goods
     * @return
     */
    Integer updateGoods(Goods goods);

    /**
     * 物品导出
     * @param titles
     * @param goodsList
     * @return
     */
    HSSFWorkbook goodsExport(String[] titles, List<Goods> goodsList);
}
