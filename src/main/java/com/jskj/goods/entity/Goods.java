package com.jskj.goods.entity;

import lombok.Data;

@Data
public class Goods {

    private Integer id;

    private String goodsName;

    private Integer typeId;

    private String typeName;

    private Integer count;

    // 备注
    private String remark;

    private Integer isDel;

    // 第几页
    private Integer page;

    // 每页展示多少条
    private Integer limit;

    private Integer pageIndex;

    public Integer getPageIndex() {
        if(limit != null && page != null) {
            return (page - 1)* limit;
        }
        return pageIndex;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", goodsName='" + goodsName + '\'' +
                ", typeId=" + typeId +
                ", typeName='" + typeName + '\'' +
                ", count=" + count +
                ", isDel=" + isDel +
                ", page=" + page +
                ", limit=" + limit +
                '}';
    }
}
